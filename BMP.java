import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BMP {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		File img = new File("D:/kip-bmp.bmp");
		
		FileInputStream reader = new FileInputStream(img);
		
		byte[] data = new byte[(int)img.length()];
		reader.read(data);
		byte[] pixels = {data[28], data[29]};
		byte[] horizontal = {data[18], data[19], data[20], data[21]};
		byte[] vertical = {data[22], data[23], data[24], data[25]};
		
		ByteBuffer b1 = ByteBuffer.wrap(pixels);
		b1.order(ByteOrder.LITTLE_ENDIAN);
		ByteBuffer b2 = ByteBuffer.wrap(horizontal);
		b2.order(ByteOrder.LITTLE_ENDIAN);
		ByteBuffer b3 = ByteBuffer.wrap(vertical);
		b3.order(ByteOrder.LITTLE_ENDIAN);
	
		
		int deep = b1.getShort();
		deep = deep/8;
		int hor = b2.getInt();
		int ver = b3.getInt();
		String name = img.getName();
		
		FileOutputStream writer = new FileOutputStream("D:/rev-"+name);
	
		
		
		for(int i=0; i<54; i++){
			writer.write(data[i]);
		}
		
		for(int i = hor*ver*deep + 54; i>=54+hor; i-=hor*deep){
			for(int j=i-hor*deep; j<i; j++){
				//System.out.println(j);
				writer.write(data[j]);
			}
		}
		
		writer.close();
	

	}

}
