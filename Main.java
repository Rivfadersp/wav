
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Main {
	
	public static void main(String[] args)throws IOException{
		File music = new File("D:/jocelyn-pook-masked-ball-eyes-wide-shut-ost_fmusic_mobi.wav");
		FileInputStream reader = new FileInputStream(music);
		byte[] data = new byte[(int)music.length()];
		
		reader.read(data);
		byte[] datasizeinf = {data[40], data[41], data[42], data[43]};
		byte[] samplenghtinf = {data[32], data[33]};
		ByteBuffer b1 = ByteBuffer.wrap(datasizeinf);
		b1.order(ByteOrder.LITTLE_ENDIAN);
		ByteBuffer b2 = ByteBuffer.wrap(samplenghtinf);
		b2.order(ByteOrder.LITTLE_ENDIAN);
		
		int datasize = b1.getInt();
		int sample = b2.getShort();
		
		String revname = "rev - " + music.getName();
		FileOutputStream writer = new FileOutputStream("D:/"+ revname);
		
		for(int i=0; i<44; i++){
			writer.write(data[i]);
		}
		
		for(int i = datasize+44; i>=44; i-=sample){
			for(int j=i-sample; j<i; j++){
				writer.write(data[j]);
			}
		}
		
		writer.close();
	
	}
}
